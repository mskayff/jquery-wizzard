(function($){
	
	$.fn.wizzard = function(options){

		var current_frame = 0;
		var breadcrumbs = [];

		var $frames = options.frames;

		$('.btn-next').click(function(e){		
			toggleWizzardFrame('up');
		});

		$('.btn-prev').click(function(e){
			toggleWizzardFrame('down');
		});		

		/**
		 * Process the breadcrumb
		 */
		 if(options.breadcrumb){

		 	var $breadcrumb = $("#wizzard-breadcrumb");	 	

			var extraCrumb = options.breadcrumb.lastStep? 1 : 0;	 	
		 	
		 	var bcItemWidth = 100 / (options.breadcrumb.items.length + extraCrumb);
		 	for(var i=0;i<options.breadcrumb.items.length;i++){
		 		addBreadcrumbElement(options.breadcrumb.items[i], i);
		 	}

		 	if(options.breadcrumb.lastStep){
		 		addBreadcrumbElement(options.breadcrumb.lastStepLabel, i+1);
		 	}
		 }

		 // Check prescense of numeric hash
		 var hash = window.location.hash.substring(1);

		 if($.isNumeric(hash)){		 	
		 	// Go to this frame when present on hash
		 	goToFrame(hash);
		 }

		 function toggleWizzardFrame(move){

		 	resetCurrentFrameActive();
			
			if(move == 'up'){
				current_frame++;
			}else{
				current_frame--;
			}
			$(breadcrumbs[current_frame]).removeClass('wizzard-bc-inactive');
			$(breadcrumbs[current_frame]).addClass('wizzard-bc-active');
			$('#' + $frames[current_frame]).fadeIn(300).addClass('active');
		}


		/**
		 * Go to this frame
		 */
		function goToFrame(frame_num){
			// Cannot go to an inexsitent frame
			if($frames.length < frame_num)
				return false;

			// Adjust frame_num to array index based
			var frame_index = frame_num - 1;

			resetCurrentFrameActive();

			$(breadcrumbs[frame_index]).removeClass('wizzard-bc-inactive');
			$(breadcrumbs[frame_index]).addClass('wizzard-bc-active');
			$('#' + $frames[frame_index]).fadeIn(300).addClass('active');
		}

		/**
		 * Reset all current active classes
		 */		
		function resetCurrentFrameActive(){
			$('#' + $frames[current_frame]).removeClass('active').hide();
			$(breadcrumbs[current_frame]).removeClass('wizzard-bc-active');
			$(breadcrumbs[current_frame]).addClass('wizzard-bc-inactive');
		}


		function addBreadcrumbElement(name, index){
		 	var baseOverlap = 10;
		 	var bcElement = document.createElement("div");
		 		bcElement.style.width = bcItemWidth + '%';
		 		if(index == 0){
		 			$(bcElement).addClass('wizzard-bc-active');
		 		}
		 		else{
		 			$(bcElement).addClass('wizzard-bc-inactive');
		 			var marginleft = (index + 1) * baseOverlap;
		 			$(bcElement).css('marginLeft', '-' + marginleft + 'px');
		 		}
		 		$(bcElement).addClass('wizzard-breadcrumb-item')
		 					.css('z-index', 1000 - (index + 10))
		 					.html(name);

		 	breadcrumbs.push(bcElement);
		 	$breadcrumb.append(bcElement);
		 }
		 
	};


})(jQuery);